/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * 运行此例程的命令
 * ./Examples/Monocular/mono_tum Vocabulary/ORBvoc.txt Examples/Monocular/TUM1.yaml /home/eskimo/Tools/rgbd_dataset_freiburg1_xyz
 * 命令参数解释：
 * 运行的目标文件夹是ORB_SLAM2文件夹目录
 * 第一个参数是运行的可执行文件的路径
 * 第二个参数是词袋的保存路径
 * 第三个参数是使用的数据集的相机标定参数
 * 第四个路径是使用的数据集的路径
 * 注：运行命令只适用于本机，如果移植拷贝到其他环境时，需要做修改
 */


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

#include<System.h>
#include "math.h"

using namespace std;
using namespace cv;

void LoadImages(const string &strFile, vector<string> &vstrImageFilenames,
                vector<double> &vTimestamps);

void MyGammaCorrection(Mat& src, Mat& dst, float fGamma);



int main(int argc, char **argv)
{
    if(argc != 4)
    {
        cerr << endl << "Usage: ./mono_tum path_to_vocabulary path_to_settings path_to_sequence" << endl;
        return 1;
    }

    // Retrieve paths to images
    vector<string> vstrImageFilenames;
    vector<double> vTimestamps;
    string strFile = string(argv[3])+"/rgb.txt";
    
    //装载rgb.txt文件，从而读取到图像的编号以及名称序列，并存进时间戳数组以及文件名数组中
    LoadImages(strFile, vstrImageFilenames, vTimestamps);
	
    //得到数据集图片的数量
    int nImages = vstrImageFilenames.size();

    //创建SLAM系统对象，参数为词袋路径+相机参数文件路径+相机数据模式+是否创建窗口查看 
    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    ORB_SLAM2::System SLAM(argv[1],argv[2],ORB_SLAM2::System::MONOCULAR,true);

    // Vector for tracking time statistics定义时间状态数组用于存放每一帧图像装载的时间花费
    vector<float> vTimesTrack;
    vTimesTrack.resize(nImages); //重新设置数组的长度为图片数量

    cout << endl << "-------" << endl;
    cout << "Start processing sequence ..." << endl;
    cout << "Images in the sequence: " << nImages << endl << endl;

    // Main loop开始主循环
    cv::Mat im;
    for(int ni=0; ni<nImages; ni++)
    {
        // Read image from file
        im = cv::imread(string(argv[3])+"/"+vstrImageFilenames[ni],CV_LOAD_IMAGE_UNCHANGED);
        double tframe = vTimestamps[ni];//读出该帧的时间戳，其实就是文件编号

        //处理异常情况
        if(im.empty())
        {
            cerr << endl << "Failed to load image at: "
                 << string(argv[3]) << "/" << vstrImageFilenames[ni] << endl;
            return 1;
        }

        //读取输入图像之前的系统时间，分别处理C11标准和非C11标准的情况
#ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
#else
        std::chrono::monotonic_clock::time_point t1 = std::chrono::monotonic_clock::now();
#endif

		//float fGamma=1/1.4;
	    //MyGammaCorrection(im, im, fGamma);
        // Pass the image to the SLAM system
        SLAM.TrackMonocular(im,tframe);

	//读取这一帧图像处理之后的系统时间
#ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
#else
        std::chrono::monotonic_clock::time_point t2 = std::chrono::monotonic_clock::now();
#endif

	//计算这一帧图像跟踪所花费的时间
        double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

        vTimesTrack[ni]=ttrack;  //把该帧图像处理花费的时间存进数组

        // Wait to load the next frame
        //这里的等待时间其实有一些不科学，在跑自己的数据及的时候，可以不用这么处理
        //这里的时间等待主要是为了处理tum数据集的，自己的数据集一般是直接把图像按照标号顺序命名，时间戳这个概念就不是那么重要
        double T=0;
        if(ni<nImages-1)
            T = vTimestamps[ni+1]-tframe;
        else if(ni>0)
            T = tframe-vTimestamps[ni-1];

        if(ttrack<T)
            usleep((T-ttrack)*1e6);
    }

    // Stop all threads
    SLAM.Shutdown();

    // Tracking time statistics
    sort(vTimesTrack.begin(),vTimesTrack.end());
    float totaltime = 0;
    for(int ni=0; ni<nImages; ni++)
    {
        totaltime+=vTimesTrack[ni];
    }
    cout << "-------" << endl << endl;
    cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
    cout << "mean tracking time: " << totaltime/nImages << endl;

    // Save camera trajectory
    SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");

    return 0;
}

void LoadImages(const string &strFile, vector<string> &vstrImageFilenames, vector<double> &vTimestamps)
{
    ifstream f;
    f.open(strFile.c_str());

    // skip first three lines
    //这里没有搞懂为什么要舍弃掉前面三张图，是因为数据集的原因？
    string s0;
    getline(f,s0);
    getline(f,s0);
    getline(f,s0);

    //这下面使用的stringstream对象来处理从文件中读出的每一行数据，
    //并切分成double类型和string类型分别存储，可以说是很简约的实现了，值得借鉴
    while(!f.eof())
    {
        string s;
        getline(f,s);
	
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            double t;
            string sRGB;
            ss >> t;
            vTimestamps.push_back(t);
            ss >> sRGB;
            vstrImageFilenames.push_back(sRGB);
        }
    }
}


void MyGammaCorrection(Mat& src, Mat& dst, float fGamma)  
{  
  
    // build look up table  
    unsigned char lut[256];  
    for( int i = 0; i < 256; i++ )  
    {  
        lut[i] = saturate_cast<uchar>(pow((float)(i/255.0), fGamma) * 255.0f);  
    }  
  
    dst = src.clone();  
    const int channels = dst.channels();  
    switch(channels)  
    {  
        case 1:   //灰度图的情况
            {  
  
                MatIterator_<uchar> it, end;  
                for( it = dst.begin<uchar>(), end = dst.end<uchar>(); it != end; it++ )  
                    //*it = pow((float)(((*it))/255.0), fGamma) * 255.0;  
                    *it = lut[(*it)];  
  
                break;  
            }  
        case 3:  //彩色图的情况
            {  
  
                MatIterator_<Vec3b> it, end;  
                for( it = dst.begin<Vec3b>(), end = dst.end<Vec3b>(); it != end; it++ )  
                {  
                    //(*it)[0] = pow((float)(((*it)[0])/255.0), fGamma) * 255.0;  
                    //(*it)[1] = pow((float)(((*it)[1])/255.0), fGamma) * 255.0;  
                    //(*it)[2] = pow((float)(((*it)[2])/255.0), fGamma) * 255.0;  
                    (*it)[0] = lut[((*it)[0])];  
                    (*it)[1] = lut[((*it)[1])];  
                    (*it)[2] = lut[((*it)[2])];  
                }  
  
                break;  
  
            }  
    }  
}
